from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError

debug = False

import os
import sys
import time
import datetime
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST

from pytz import timezone
try:
    import ephem
    EPHEM_AVAILABLE = True
except:
    EPHEM_AVAILABLE = False

import td
import ctypes

PLUGINS_PATH = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins')

sys.path.append(PLUGINS_PATH)

from general import getSetting

class Command(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    cmd = models.CharField(max_length=15)
    argInt = models.PositiveIntegerField(default = 0)

    def __unicode__(self):
        if self.id > 100:
            return self.cmd
        else:
            return self.cmd + ' ' + str(self.argInt)
        
    def getExecCmd(self, deviceId):
        if self.cmd == 'ACTIVATE':
            print 'Should never reach getExecCmd for cmd type ACTIVATE'
            return ''
        elif self.cmd == 'DIM':
            return '--dimlevel ' + str(int(2.55*self.argInt)) + ' --dim ' + str(deviceId)
        else:
            return '--' + self.cmd.lower() + ' ' + str(deviceId)

def validateInt(f, max, min, protocol, field):
    try:
        x = int(f)
    except:
        raise ValidationError('For %s %s should be an integer %d-%d' %(protocol, field, min, max))
    if x < min or x > max:
        raise ValidationError('For %s %s should be an integer %d-%d' %(protocol, field, min, max))

def validateCharIn(f, allowed, nrChars, protocol, field):
    if len(f) != nrChars:
        raise ValidationError('For %s %s should be %d characters' %(protocol, field))
    for c in f:
        if c not in allowed:
            raise ValidationError('For %s %s should only be characters from %s' %(protocol, field, allowed))
                              
    
def validateEmpty(f, protocol, field):
    if f != '':
        raise ValidationError('For %s %s should be empty' %(protocol, field))
            
class Device(models.Model):

    name = models.CharField(max_length=40, unique = True)
    htmlText = models.CharField(max_length=40, unique = True, verbose_name = 'Display text')
    order = models.PositiveIntegerField(default = 100)
     
    activate = models.BooleanField()
    onOff = models.BooleanField()
    dim = models.BooleanField()
    hidden = models.BooleanField()
    
    def __unicode__(self):
        return self.name

    def containDevice(self, devicesNotAllowed):
        print 'ContainDevice called for ', repr(self)
        devicesNotAllowed.append(self)
        
        if hasattr(self, 'rawtellstickdevice'):
            return self.rawtellstickdevice.containRawDevice(devicesNotAllowed)
        elif hasattr(self, 'groupdevice'):
            return self.groupdevice.containGroupDevice(devicesNotAllowed)
        elif hasattr(self, 'timerdevice'):
            return self.timerdevice.containTimerDevice(devicesNotAllowed)
        elif hasattr(self, 'preset'):
            return self.preset.containPresetDevice(devicesNotAllowed)
        else:
            print 'unknown instance type in containDevice', self
        return False


        
    def execute(self, cmd):

        if cmd.cmd == 'ON' and not self.onOff:
            return False
        elif cmd.cmd == 'OFF' and not self.onOff:
            return False
        elif cmd.cmd == 'DIM' and not self.dim:
            return False
        elif cmd.cmd == 'ACTIVATE' and not self.activate:
            return False
            
        if hasattr(self, 'rawtellstickdevice'):
            return self.rawtellstickdevice.doExecute(cmd)
        elif hasattr(self, 'sendsignaldevice'):
            return self.sendsignaldevice.doExecute(cmd)
        elif hasattr(self, 'groupdevice'):
            return self.groupdevice.doExecute(cmd)
        elif hasattr(self, 'timerdevice'):
            return self.timerdevice.doExecute(cmd)
        elif hasattr(self, 'preset'):
            return self.preset.doExecute(cmd)
        elif hasattr(self, 'woldevice'):
            return self.woldevice.doExecute(cmd)
        else:
            print 'unknown instance type'
        return False
    

class RawTellstickDevice(Device):
    PROTOCOLCHOICES = (
        ('arctech', (
                ('arctech;codeswitch', 'arctech codeswitch'),
                ('arctech;bell', 'arctech bell'),
                ('arctech;selflearning-switch', 'arctech selflearning-switch'),
                ('arctech;selflearning-dimmer', 'arctech selflearning-dimmer'))),
        ('risingsun', (
                ('risingsun;codeswitch', 'risingsun codeswitch'),
                ('risingsun;selflearning', 'risingsun selflearning'))),
        ('silvanchip', (
                ('silvanchip;ecosavers', 'silvanchip ecosavers'),
                ('silvanchip;kp100', 'silvanchip kp100'))),
        ('others', (
                ('brateck;', 'brateck'),
                ('everflourish;', 'everflourish'),
                ('fuhaote;', 'fuhaote'),
                ('hasta;', 'hasta'),
                ('sartano;', 'sartano'),
                ('upm;', 'upm'),
                ('waveman;', 'waveman'),
                ('x10;', 'x10'),
                ('yidong;', 'yidong'))))                  
         
    deviceId = models.PositiveIntegerField(default = 9999)

    rawName = models.CharField(max_length=100, default = '', verbose_name = 'Name in tellstick config')
    protocol = models.CharField(max_length=40, default = '', choices=PROTOCOLCHOICES)
    house = models.CharField(max_length=20, default = '', blank=True)
    unit = models.CharField(max_length=20, default = '', blank=True)
    code = models.CharField(max_length=20, default = '', blank=True)

    def containRawDevice(self, device):
        print 'ContainRawDevice for ', repr(self)
        return False

    def clean(self):
        self.activate = False
        self.onOff = True
                
        #Extend this list for device types that support dimming
        if self.protocol in ['arctech;selflearning-dimmer']:
            self.dim = True
        else:
            self.dim = False

        #Need to make sure house,unit, code is set/unset according to protocol/model
        if self.protocol in ['arctech;codeswitch', 'x10;']:
            validateInt(self.unit, 16, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateCharIn(self.house, 'ABCDEFGHIJKLMNOP', 1, self.protocol, 'house')

        elif self.protocol == 'arctech;bell':
            validateEmpty(self.unit, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateCharIn(self.house, 'ABCDEFGHIJKLMNOP', 1, self.protocol, 'house')

        elif self.protocol in ['arctech;selflearning-dimmer', 'arctech;selflearning-switch']:
            validateInt(self.unit, 16, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 67108863, 1, self.protocol, 'house')
        elif self.protocol == 'brateck;':
            validateEmpty(self.unit, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateCharIn(self.house, '10-', 8, self.protocol, 'house')
        elif self.protocol == 'everflourish;':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 16383, 0, self.protocol, 'house')
        elif self.protocol in ['fuhaote;', 'sartano;']:
            validateEmpty(self.unit, self.protocol, 'unit')
            validateCharIn(self.code, '10', 10, self.protocol, 'code')
            validateEmpty(self.house, self.protocol, 'house')
        elif self.protocol == 'hasta;':
            validateInt(self.unit, 15, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 65536, 1, self.protocol, 'house')
        elif self.protocol == 'risingsun;codeswitch':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 4, 1, self.protocol, 'house')
        elif self.protocol == 'risingsun;selflearning':
            validateInt(self.unit, 16, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 33554432, 1, self.protocol, 'house')
        elif self.protocol == 'silvanship;ecosavers':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 1048575, 1, self.protocol, 'house')
        elif self.protocol == 'silvanchip;kp100':
            validateEmpty(self.unit, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 1048575, 1, self.protocol, 'house')
        elif self.protocol == 'upm;':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 4095, 1, self.protocol, 'house')
        elif self.protocol == 'yidong;':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateEmpty(self.house, self.protocol, 'house')
        else:
            raise ValidationError('Protocol: %s not supported' %(self.protocol))

        
        if self.deviceId != 9999:
            if debug:
                print 'Remove device', repr(self.deviceId), td.removeDevice(int(self.deviceId))
        else:
            if debug:
                print 'No remove necessary', self.deviceId

        devId = td.addDevice()
        if devId >= 0:
            self.deviceId = devId
        else:
            raise ValidationError('Unable to addDevice to tellstick configuration. Got the following error:' + td.getErrorString(devId))
                
        td.setName(devId, str(self.rawName))
        prot = str(self.protocol)
        td.setProtocol(devId, prot.split(';')[0])
        if len(self.protocol.split(';')) == 2:
            td.setModel(devId, prot.split(';')[1])
        
        if self.house != '':
            td.setDeviceParameter(devId, 'house', str(self.house))
        if self.code != '':
            td.setDeviceParameter(devId, 'code', str(self.code))
        if self.unit != '':
            td.setDeviceParameter(devId, 'unit', str(self.unit))

    def delete(self):
        if debug:
            print 'Remove device on delete'
        td.removeDevice(int(self.deviceId))
        super(RawTellstickDevice, self).delete()
            
    
    def doExecute(self, cmd):
        if debug:
            print 'RawTellstickDevice doExecute', self.deviceId, self.name, cmd

        postToQueue('tellstick,do:' + str(self.deviceId) + ',' + str(cmd.cmd) +',' + str(cmd.argInt), 'web')

        return True

    def getConf(self):
        s = 'device {\n'
        s += '  id = ' + str(self.deviceId) + '\n'
        s += '  name = "' + self.rawName + '"\n'
        s += '  protocol = "' + self.protocol.split(';')[0] + '"\n'
        if self.protocol.split(';')[1] != '':
            s += '  model = "' + self.protocol.split(';')[1] + '"\n'
        s += '  parameters {\n'
        if self.house != '':
            s += '    house = "' + self.house + '"\n'
        if self.unit != '':
            s += '    unit = "' + self.unit + '"\n'
        if self.code != '':
            s += '    code = "' + self.code + '"\n'
        s += '  }\n'
        s += '}\n'

        return s
        
class TimerDevice(Device):
    subDevice = models.ForeignKey(Device, related_name='subdevice')
    delayMinutes = models.PositiveIntegerField(default = 1)
    command = models.ForeignKey(Command, default = 102)
    condition = models.ForeignKey('GlobalVariable', blank = True, null = True)

    def containTimerDevice(self, device):
        return self.subDevice.containDevice(device)

    def save(self):
        self.activate = True
        self.onOff = False
        self.dim = False
        super(TimerDevice, self).save()

    def doExecute(self, cmd):
        if debug:
            print 'TimerDevice doExecute'
        local = timezone(settings.TIME_ZONE)
        e = Event(device = self.subDevice,
                  command = self.command,
                  doAt = local.localize(datetime.datetime.now()) + datetime.timedelta(minutes = self.delayMinutes),
                  condition = self.condition)
        e.save()



class GroupDevice(Device):
    subDevices = models.ManyToManyField(Device, related_name='subdevices')

#    def clean(self):
#       print 'Clean called with the following subdevices', repr(self)
#       for dev in self.subDevices.all():
#            print repr(dev)
 
 
#        if self.containGroupDevice([self]):
#            raise ValidationError('References back to itself is detected and not allowed. Make sure there is no ring of device references configured.')


    def containGroupDevice(self, devicesNotAllowed):
        print 'ContainGroupDevice on ', repr(self)

        print 'Not allowed devices', devicesNotAllowed
        for dev in self.subDevices.all():
            print 'Now testing ', repr(dev)
            if dev in devicesNotAllowed:
                return True



            print 'Contain', repr(dev)
            if dev.containDevice(devicesNotAllowed):
                print 'True'
                return True
        return False


    def save(self):
        self.activate = False
        self.onOff = True        
        self.dim = True
        super(GroupDevice, self).save()
        for subdev in self.subDevices.all():
            if not subdev.dim:
                self.dim = False
                super(GroupDevice, self).save()
                break
            
    def doExecute(self, cmd):
        for dev in self.subDevices.all():
            dev.execute(cmd)


class Preset(Device):
    def save(self):
        self.activate = True
        self.onOff = False
        self.dim = False
        super(Preset, self).save()

    def containPresetDevice(self, device):
        for dev in PresetEntry.objects.filter(presetDevice = self):
            if dev.containDevice(device):
                return True
        return False


    def doExecute(self, cmd):
        if debug:
            print 'Preset doExecute'

        for p in PresetEntry.objects.filter(presetDevice = self):
            if debug:
                print p
            p.device.execute(p.command)
            
class PresetEntry(models.Model):
    order = models.PositiveIntegerField(default = 1)
    device = models.ForeignKey(Device)
    command = models.ForeignKey(Command, default = 102)
    presetDevice = models.ForeignKey(Preset, related_name = 'entries')

    def __unicode__(self):
        return self.device.__unicode__() + ' ' + self.command.__unicode__()


class SendSignalDevice(Device):
    signalOn = models.CharField(max_length=100, default = '', verbose_name = 'Signal to send at ON/Activate')
    signalOff = models.CharField(max_length=100, default = '', verbose_name = 'Signal to send at OFF', blank = True)

    def clean(self):
        if self.signalOff != '':
            self.activate = False
            self.onOff = True
        else:
            self.activate = True
            self.onOff = False

    def doExecute(self, cmd):
        if debug:
            print 'SendSignalDevice doExecute', self.name, cmd

        if cmd.cmd == 'ON' or cmd.cmd == 'ACTIVATE':
            postToQueue(self.signalOn, 'SendSignalDevice')
        elif cmd.cmd == 'OFF':
            if self.signalOff:
                postToQueue(self.signalOff, 'SendSignalDevice')

        return True

    def getConf(self):
        return ''

class WolDevice(Device):
    mac = models.CharField(max_length=17, default = '', verbose_name = 'MAC address to send to')
    ip = models.CharField(max_length=15, default = '', verbose_name = 'IP address to send to (blank for broadcast)', blank = True)

    def clean(self):
        self.activate = True
        self.onOff = False
        if len(self.mac) != 17:
            raise ValidationError('MAC address needs to be 6 pair of digits hexadecimal (0-F) sepparated by : or -')

        self.mac = self.mac.replace('-', ':')
        try:
            for m in self.mac.split(':'):
                x = int(m, 16)
        except:
            raise ValidationError('MAC address needs to be 6 pair of digits hexadecimal (0-F) sepparated by : or -')

        if self.ip != '':
            ipParts = self.ip.split('.')
            if len(ipParts) != 4:
                raise ValidationError('IP address needs to be blank (for broadcast) or a valid ip address like "192.168.1.20"')
            try:
                for p in ipParts:
                    x = int(p, 10)
            except:
                raise ValidationError('IP address needs to be blank (for broadcast) or a valid ip address like "192.168.1.20"')
            

    def doExecute(self, cmd):
        if debug:
            print 'Wol Device doExecute', self.name, cmd

        mac_raw = ''
        for m in self.mac.split(':'):
            x = int(m, 16)
            mac_raw += chr(x)


        wolpacket = '\xFF\xFF\xFF\xFF\xFF\xFF' + mac_raw * 16
        if debug:
            print 'MAC raw', repr(mac_raw)
            print 'WOL packet', repr(wolpacket)

        if self.ip == '':
            dest = '<broadcast>'
        else:
            dest = self.ip

        try:
            sock = socket(AF_INET, SOCK_DGRAM)
            sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
            sock.sendto(wolpacket, (dest, 9))
            sock.close()
        except:
            print 'Error sending WOL packet'
            if debug:
                raise

        return True

    def getConf(self):
        return ''

    
class ScheduledEvent(models.Model):
    RELATIVE_CHOICES = ((0, 'Absolute time'),
                        (1, 'Relative sunrise'),
                        (2, 'Relative sunset'))

    device = models.ForeignKey(Device)
    command = models.ForeignKey(Command)


    relativeTo = models.PositiveIntegerField(default = 0, choices = RELATIVE_CHOICES)
    doAt = models.TimeField(verbose_name = 'Execution time', default = datetime.time(hour = 6, minute = 0))

    relativeTime = models.IntegerField(default = 0, verbose_name = 'Minutes relative to sunset/sunrise')

    doMonday = models.BooleanField(verbose_name = 'Monday')
    doTuesday = models.BooleanField(verbose_name = 'Tuesday')
    doWednesday = models.BooleanField(verbose_name = 'WednesDay')
    doThursday = models.BooleanField(verbose_name = 'Thursday')
    doFriday = models.BooleanField(verbose_name = 'Friday')
    doSaturday = models.BooleanField(verbose_name = 'Saturday')
    doSunday = models.BooleanField(verbose_name = 'Sunday')    

    condition = models.ForeignKey('GlobalVariable', blank = True, null = True)

    def clean(self):
        if self.relativeTo > 0 and not EPHEM_AVAILABLE:
            raise ValidationError('pyephem not installed (correctly), that is required for relative to sunrise/sunset')
        if abs(self.relativeTime) > (8*60):
            raise ValidationError('Relative Time cant be greater than 8*60=480 minutes')
        if self.relativeTo == 0:
            self.realtiveTime = 0
        else:
            self.doAt = datetime.time(hour = 6, minute = 0)

    def __unicode__(self):
        weekdays = ''
        if self.doMonday:
            weekdays += 'M'
        else:
            weekdays += '-'

        if self.doTuesday:
            weekdays += 'T'
        else:
            weekdays += '-'
        if self.doWednesday:
            weekdays += 'W'
        else:
            weekdays += '-'
        if self.doThursday:
            weekdays += 'T'
        else:
            weekdays += '-'
        if self.doFriday:
            weekdays += 'F'
        else:
            weekdays += '-'
        if self.doSaturday:
            weekdays += 'S'
        else:
            weekdays += '-'
        if self.doSunday:
            weekdays += 'S'
        else:
            weekdays += '-'

        return self.device.__unicode__() + ' ' + self.command.__unicode__() + ' ' + weekdays + ' @ ' + self.doAt.isoformat()

    def scheduleNext(self, earliest = None):
        
        for e in Event.objects.filter(schedule = self):
            if debug:
                print 'Deleteing', e
            e.delete()

        local = timezone(settings.TIME_ZONE)

        if not earliest:
            earliest = local.localize(datetime.datetime.now())

        weekdayNow = earliest.weekday()
        if debug:
            print 'Earliest:', earliest
            print 'Earliest day:', weekdayNow
            print 'Relative to:', self.relativeTo

        every = [self.doMonday,
                 self.doTuesday,
                 self.doWednesday,
                 self.doThursday,
                 self.doFriday,
                 self.doSaturday,
                 self.doSunday]

        if self.relativeTo == 0:
            if earliest.time() < self.doAt:
                timeDoAt = datetime.datetime.combine(earliest.date(), self.doAt)
            else:
                timeDoAt = datetime.datetime.combine(earliest.date(), self.doAt) + datetime.timedelta(days = 1)
        else:
            try:
                o=ephem.Observer()
                o.lat=str(getSetting('Location Latitude'))
                o.long=str(getSetting('Location Longitude'))
                o.date = ephem.Date(earliest - datetime.timedelta(minutes = self.relativeTime))
                s=ephem.Sun()
                s.compute()
                if self.relativeTo == 1:
                    timeDoAt = ephem.localtime(o.next_rising(s))
                elif self.relativeTo == 2:
                    timeDoAt = ephem.localtime(o.next_setting(s))
            except:
                print 'Error calculating sunset/sunrise based on location'
                raise


        daysToNext = (timeDoAt.date() - earliest.date()).days
        weekdayNow = timeDoAt.weekday()

        if debug:
            print 'timeDoAt', timeDoAt
            print 'Every:', every
            print 'WeekdayNow', weekdayNow
            print 'DaysToNext', daysToNext

        for i in range(weekdayNow, 12):
            if every[i%7]:
                break
            daysToNext += 1
        else:
            print 'Shouldnt be done any time'
            return

        if debug:
            print 'Days to next', daysToNext

        if self.relativeTo == 0:
            newDateTime = local.localize(datetime.datetime.combine(earliest.date() + datetime.timedelta(days = daysToNext), self.doAt))

        elif self.relativeTo == 1:
            o.date = datetime.datetime.combine(earliest.date() + datetime.timedelta(days = (daysToNext - 1)), datetime.time(hour = 20, minute = 0))
            newDateTime = local.localize(ephem.localtime(o.next_rising(s))) + datetime.timedelta(minutes = self.relativeTime)
            if debug:
                print o.date
                                               

        elif self.relativeTo == 2:
            o.date = datetime.datetime.combine(earliest.date() + datetime.timedelta(days = daysToNext), datetime.time(hour = 8, minute = 0))
            newDateTime = local.localize(ephem.localtime(o.next_setting(s))) + datetime.timedelta(minutes = self.relativeTime)
            if debug:
                print o.date
            
        if debug:
            print 'Next event', newDateTime

        if newDateTime < local.localize(datetime.datetime.now()):
            print '\n\nBAD DATE, already passed\n'

        e = Event(device = self.device, command = self.command, doAt = newDateTime, schedule = self, condition = self.condition)
        e.save()




class Event(models.Model):
    device = models.ForeignKey(Device)
    command = models.ForeignKey(Command)
    doAt = models.DateTimeField()
    condition = models.ForeignKey('GlobalVariable', blank = True, null = True)

    schedule = models.ForeignKey(ScheduledEvent, null = True, blank = True, default = None)

    def __unicode__(self):
        fmt = '%Y-%m-%d %H:%M:%S'
        local = timezone(settings.TIME_ZONE)
        return self.device.htmlText + ' ' + self.command.__unicode__() + ' ' + self.doAt.astimezone(local).strftime(fmt)

    def delay(self, minutes):
        self.doAt = self.doAt + datetime.timedelta(minutes = minutes)
        self.save()

    def save(self):
        super(Event, self).save()        
        postToQueue('timedevent,changed', 'system')


class DeviceEvent(models.Model):
    device = models.ForeignKey(RawTellstickDevice)
    command = models.ForeignKey(Command)
    doneAt = models.DateTimeField()
    external = models.BooleanField()

    def __unicode__(self):
        fmt = '%Y-%m-%d %H:%M:%S'
        local = timezone(settings.TIME_ZONE)
        return self.device.htmlText + ' ' + self.command.__unicode__() + ' ' + self.doneAt.astimezone(local).strftime(fmt)



class RawDeviceEvent(models.Model):
    data = models.CharField(max_length=100, default = '')
    controllerId = models.IntegerField()
    receivedAt = models.DateTimeField()

    def __unicode__(self):
        fmt = '%H:%M:%S'
        local = timezone(settings.TIME_ZONE)
        return self.data + ' @ ' + self.receivedAt.astimezone(local).strftime(fmt)


class SensorEventRaw(models.Model):
    protocol = models.CharField(max_length=100, default = '')
    model = models.CharField(max_length=40, default = '')
    sensor_id = models.IntegerField()
    dataType = models.IntegerField()
    value = models.CharField(max_length=20, default = '')
    timestamp = models.IntegerField()

    def __unicode__(self):
        return str(self.sensor_id) + ': ' + td.sensorValueTypeReadable.get(self.dataType, 'Unknown datatype') + '=' + self.value + ' @ ' + str(self.timestamp)



globalVariableCache = {}

class GlobalVariable(models.Model):
    DATATYPES = ((0, 'String/Not specified'),
                 (1, 'Integer'),
                 (2, 'Float'),
                 (3, 'Boolean'))

    name = models.CharField(max_length=30)
    htmlName = models.CharField(max_length=30, default = '', verbose_name = 'Display text')
    hidden = models.BooleanField(default = True, verbose_name = 'Hide from display')
    value = models.CharField(max_length=30)
    dataType = models.IntegerField(default = 0, choices = DATATYPES)
    lastUpdated = models.DateTimeField(auto_now = True, verbose_name = 'Last updated')

    def __unicode__(self):
        fmt = '%H:%M:%S'
        local = timezone(settings.TIME_ZONE)
        
        return self.name + u':' + self.getValue() + ' (' + self.getUpdated().astimezone(local).strftime(fmt) + ')'

    def clean(self):
        if self.dataType == 1:
            try:
                x = int(self.value)
                self.value = str(x)
            except:
                raise ValidationError('Value needs to be an integer')

        elif self.dataType == 2:
            try:
                x = float(self.value)
                self.value = str(x)
            except:
                raise ValidationError('Value needs to be a float')

        elif self.dataType == 3:
            if self.value.lower() in ['true', 't', '1', 'on', 'yes', 'y']:
                self.value = 'True'
            elif self.value.lower() in ['false', 'f', '0', 'off', 'no', 'n']:
                self.value = 'False'
            else:
                raise ValidationError('Value needs to be a boolean accepting the following for True (true, t, 1, on, yes, y) and False (false, f, 0, off, no, n). not case sensitive')

    def save(self):
        if self.dataType == 1:
            try:
                x = int(self.value)
                self.value = str(x)
            except:
                self.value = '-9999'

        elif self.dataType == 2:
            try:
                x = float(self.value)
                self.value = str(x)
            except:
                self.value = '-9999.99'
        elif self.dataType == 3:
            if self.value.lower() in ['true', 't', '1', 'on', 'yes', 'y']:
                self.value = 'True'
            else:
                self.value = 'False'


        self.value = self.getValue()

        super(GlobalVariable, self).save()
        try:
            postToQueue('variable_changed.' + self.name + ':' + str(self.value), 'system')
        except:
            print 'Error in save of Global Variable'

    def getValue(self):
        if self.name in globalVariableCache:
            return globalVariableCache[self.name][0]
        else:
            return self.value

    def getUpdated(self):
        if self.name in globalVariableCache:
            return globalVariableCache[self.name][1]
        else:
            return self.lastUpdated

    def updateValue(self, value):
        if self.dataType == 1:
            try:
                x = int(value)
                value = str(x)
            except:
                value = '-9999'

        elif self.dataType == 2:
            try:
                x = float(value)
                value = str(x)
            except:
                value = '-9999.99'

        elif self.dataType == 3:
            if value.lower() in ['true', 't', '1', 'on', 'yes', 'y']:
                value = 'True'
            else:
                value = 'False'


        local = timezone(settings.TIME_ZONE)

        globalVariableCache[self.name] = (value, local.localize(datetime.datetime.now()))
        try:
            postToQueue('variable_changed.' + self.name + ':' + str(value), 'system')
        except:
            print 'Error in save of Global Variable'

        
            
        
        


from signals.models import postToQueue


@receiver(post_save, dispatch_uid=__name__, sender=ScheduledEvent)
def scheduledEventChanged(sender, **kwargs):
    kwargs['instance'].scheduleNext()


