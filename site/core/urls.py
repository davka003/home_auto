from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('core.views',
    url(r'^$', 'index', kwargs = {'admin': False}),
    url(r'admin/^$', 'index', kwargs = {'admin': True}),

    url(r'^(?P<device_id>\d+)/$', 'detail'),

    url(r'^(?P<device_id>\d+)/(?P<cmd_id>\d+)/$', 'do', kwargs = {'learn_admin': False}),
    url(r'^learn/(?P<device_id>\d+)/(?P<cmd_id>\d+)/$', 'do', kwargs = {'learn_admin': True}),

    url(r'^log/$', 'log'),

    url(r'^events/$', 'event_index', kwargs = {'admin': False}),
    url(r'^events/admin/$', 'event_index', kwargs = {'admin': True}),

    url(r'^events/(?P<event_id>\d+)/$', 'event', kwargs = {'admin': False}),
    url(r'^events/admin/(?P<event_id>\d+)/$', 'event', kwargs = {'admin': True}),

    url(r'^events/(?P<event_id>\d+)/delay/(?P<minutes>\d+)/$', 'delay_event', kwargs = {'admin': False}),
    url(r'^events/admin/(?P<event_id>\d+)/delay/(?P<minutes>\d+)/$', 'delay_event', kwargs = {'admin': True}),

    url(r'^events/(?P<event_id>\d+)/cancel$', 'cancel_event', kwargs = {'admin': False}),
    url(r'^events/admin/(?P<event_id>\d+)/cancel$', 'cancel_event', kwargs = {'admin': True}),

    url(r'^api/devices/$', 'api_devices'),

    url(r'^learn/$', 'learn', kwargs = {'admin': False}),
    url(r'^learn/admin/$', 'learn', kwargs = {'admin': True}),

    url(r'^backup/$', 'backup'),
    url(r'^backup/do/$', 'doBackup'),
    url(r'^backup/dorestore/(?P<backup>\S+)/$', 'restoreBackup'),
    url(r'^backup/(?P<backup>\S+)/$', 'showBackup'),

    url(r'^read_config/$', 'read_config'),
)
