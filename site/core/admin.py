from core.models import Event, Command, Device, RawTellstickDevice, TimerDevice, GroupDevice, Preset, PresetEntry, ScheduledEvent, RawDeviceEvent, SensorEventRaw, GlobalVariable, SendSignalDevice, WolDevice
from django.forms import ModelForm
from django.contrib import admin

#Remove uncesseray django stuff from admin interface
from django.contrib.sites.models import Site
from django.contrib.auth.models import User, Group
admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.unregister(Site)
#end remove

admin.site.disable_action('delete_selected')

class ScheduledEventAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
                'fields': ('device', 'command', 'relativeTo', 'doAt', 'relativeTime', 'condition')
                }),
        ('Do the following weekdays', {
            'fields': ('doMonday', 'doTuesday', 'doWednesday', 'doThursday', 'doFriday', 'doSaturday', 'doSunday')
        }),
    )


class PresetEntryInline(admin.TabularInline):
    model = PresetEntry
    fk_name = 'presetDevice'

class PresetAdmin(admin.ModelAdmin):
    exclude = ('onOff', 'dim', 'activate')
    inlines = [
        PresetEntryInline,
    ]
    ordering = ('order',)
    list_filter = ('hidden',)


class RawTellstickDeviceAdmin(admin.ModelAdmin):
    exclude = ('onOff', 'dim', 'activate', 'deviceId')
    ordering = ('order',)
    list_filter = ('hidden',)

class TimerDeviceAdmin(admin.ModelAdmin):
    exclude = ('onOff', 'dim', 'activate')
    ordering = ('order',)
    list_filter = ('hidden',)


class GroupDeviceAdmin(admin.ModelAdmin):
    exclude = ('onOff', 'dim', 'activate')
    ordering = ('order',)
    list_filter = ('hidden',)
    filter_horizontal = ('subDevices',)

class GlobalVariableAdmin(admin.ModelAdmin):
    list_display = ('htmlName', 'getValue', 'getUpdated', 'hidden')
    list_filter = ('hidden',)

class SendSignalDeviceAdmin(admin.ModelAdmin):
    exclude = ('onOff', 'dim', 'activate')
    ordering = ('order',)
    list_filter = ('hidden',)

class WolDeviceAdmin(admin.ModelAdmin):
    exclude = ('onOff', 'dim', 'activate')
    ordering = ('order',)
    list_filter = ('hidden',)


#admin.site.register(Device)
admin.site.register(Preset, PresetAdmin)
admin.site.register(ScheduledEvent, ScheduledEventAdmin)
#admin.site.register(Event)
admin.site.register(RawTellstickDevice, RawTellstickDeviceAdmin)
admin.site.register(TimerDevice, TimerDeviceAdmin)
admin.site.register(GroupDevice, GroupDeviceAdmin)
admin.site.register(SendSignalDevice, SendSignalDeviceAdmin)
admin.site.register(WolDevice, WolDeviceAdmin)
#admin.site.register(RawDeviceEvent)
#admin.site.register(SensorEventRaw)
admin.site.register(GlobalVariable, GlobalVariableAdmin)
