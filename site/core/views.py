from django.http import HttpResponse
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from core.models import Device, Command, RawTellstickDevice, GroupDevice, TimerDevice, Preset, Event, DeviceEvent, GlobalVariable
from signals.models import postToQueue
from django.conf import settings
from django.core import serializers
from django.utils import simplejson
from operator import itemgetter, attrgetter
from django import forms
from django.contrib.admin import widgets

import time
import datetime
import traceback
import sys
import os
import subprocess

debug = False

#This is hardcoded from Command model (for faster access as it never changes)
cmds = {'DIM': range(0, 101, 10), #End value is max+1 to make sure last value is included
        'ON': 101,
        'OFF': 102,
        'BELL': 103,
        'LEARN': 104,
        'ACTIVATE': 105}

iphoneDimMap = [(20, 47, 10),
                (48, 75, 20),
                (76, 103, 30),
                (104, 131, 40),
                (132, 159, 50),
                (160, 187, 60),
                (188, 215, 70),
                (216, 243, 80),
                (244, 271, 90),
                (272, 299, 100),
]
        
def getViewType(request):
    viewType = 'default'

    if 'iPhone' in request.META['HTTP_USER_AGENT']:
        viewType = 'iphone'

    viewType = 'core/' + viewType + '.html'
    return viewType


def index(request, admin):
    global cmds
    
    devList = Device.objects.filter(hidden = False).order_by('order')
    varList = GlobalVariable.objects.filter(hidden = False)

    if admin:
        template = 'admin/core_index.html'
    else:
        template = 'core/index.html'

    return render_to_response(template, {'viewType': getViewType(request), 'devList': devList, 'varList': varList, 'cmds': cmds, 'iphoneDimMap': iphoneDimMap})

class CreateEventForm(forms.ModelForm):
    class Meta:
        model = Event
        exclude = ('device', 'schedule')
    
def detail(request, device_id):
    dev = get_object_or_404(Device, pk=device_id)
    if request.method == 'POST':
        form = CreateEventForm(request.POST)

        if form.is_valid():
            e = form.save(commit = False)
            e.device = dev
            e.save()
            return redirect('/core/')

    else:
        form = CreateEventForm(initial={'device': dev.id, 'doAt': datetime.datetime.now()})
    
    return render(request, 'core/detail.html', {'viewType': getViewType(request), 'dev': dev, 'form': form})


def do(request, device_id, cmd_id, learn_admin):
    global cmds
    
    device = get_object_or_404(Device, pk=device_id)
    cmd = get_object_or_404(Command, pk=cmd_id)
    
    if debug:
        print 'do(), device', device_id, 'cmd_id', cmd_id

    device.execute(cmd)

    if learn_admin:
        return redirect('/core/learn/admin/')

    if int(cmd_id) == cmds['LEARN']:
        return redirect('/core/learn/')

    return redirect('/core/')

def learn(request, admin = False):
    global cmds
    
    devList = RawTellstickDevice.objects.all().order_by('order')
    
    if admin:
        template = 'admin/core_learn.html'
    else:
        template = 'core/learn.html'

    return render_to_response(template, {'devList': devList, 'viewType': getViewType(request), 'cmds': cmds})

def log(request):
    devEvents = DeviceEvent.objects.all().order_by('-doneAt')
    return render_to_response('core/log.html', {'devEvents': devEvents, 'viewType': getViewType(request)})


def event_index(request, admin):
    events = Event.objects.all().order_by('doAt')

    if admin:
        template = 'admin/core_event_index.html'
    else:
        template = 'core/event_index.html'
    
    return render_to_response(template, {'viewType': getViewType(request), 'events': events})


def event(request, event_id, admin):
    event = get_object_or_404(Event, pk=event_id)

    if admin:
        template = 'admin/core_event_detail.html'
    else:
        template = 'core/event_detail.html'

    return render_to_response(template, {'viewType': getViewType(request), 'event': event})

def delay_event(request, event_id, minutes, admin):
    if debug:
        print 'Delay Event', event_id
    event = get_object_or_404(Event, pk=event_id)
    event.delay(int(minutes))
    event.save()

    if admin:
        redir = '/core/events/admin/'
    else:
        redir = '/core/events/'

    return redirect(redir)

def cancel_event(request, event_id, admin):
    event = get_object_or_404(Event, pk=event_id)
    if event.schedule:
        event.schedule.scheduleNext(event.doAt)
    else:
        event.delete()

    if admin:
        redir = '/core/events/admin/'
    else:
        redir = '/core/events/'

    return redirect(redir)




def addDevice(id, name, protocol, model, house, unit, code):
    existing = RawTellstickDevice.objects.filter(house = house, unit = unit, code = code)

    if existing.count() == 0:
        if debug:
            print 'Create new RawTellstickDevice'            
        dev = RawTellstickDevice(deviceId = id,
                        name = name,
                        htmlText = name,
                        rawName = name,
                        protocol = protocol + ';' + model,
                        house = house,
                        unit = unit,
                        code = code)        
        print dev
        try:
            dev.clean()
            dev.save()
        except:
            print 'Error saving this device'
            strings = traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
            for s in strings:
                print s


    elif existing.count() >= 1:
        if debug:
            print 'Update existing RawTellstickDevice'
        dev = existing[0]

        dev.deviceId = id

        if dev.rawName == dev.name:
            dev.name = name

        if dev.rawName == dev.htmlText:
            dev.htmlText = name

        dev.rawName = name
        dev.protocol = protocol + ';' + model

        try:
            dev.clean()
            dev.save()
        except:
            print 'Error saving this device'
            strings = traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
            for s in strings:
                print s

def dumpDev(dev):
    t = {}
    t['deviceId'] = dev.id
    t['name'] = dev.name
    t['htmlText'] = dev.htmlText
    t['order'] = dev.order
    t['hidden'] = dev.hidden
    s = {}
    if dev.activate:
        s['activate'] = cmds['ACTIVATE']
    if dev.onOff:
        s['on'] = cmds['ON']
        s['off'] = cmds['OFF']
    if dev.dim:
        s['dim'] = '0-100'
        
    t['supportedCommands'] = s
    return t


def api_devices(request):
    #Need to build this to hold all information in devices and subclasses
    to_json = []
    for r in RawTellstickDevice.objects.all():        
        t = dumpDev(r)
        t['deviceType'] = 'raw'
        to_json.append(t)

    for r in GroupDevice.objects.all():        
        t = dumpDev(r)
        t['deviceType'] = 'group'
        to_json.append(t)

    for r in TimerDevice.objects.all():        
        t = dumpDev(r)
        t['deviceType'] = 'timer'
        to_json.append(t)

    for r in Preset.objects.all():        
        t = dumpDev(r)
        t['deviceType'] = 'preset'
        to_json.append(t)
  
    to_json.sort(key=lambda dev: dev['order'])

    json_serializer = serializers.get_serializer("json")()

    return HttpResponse(simplejson.dumps(to_json, indent = 2), mimetype="application/json")


class BackupUploadForm(forms.Form):
    file  = forms.FileField()
    


def niceDate(d):
    parts = d.split('_')
    return parts[0] + ' ' + parts[1][0:2] + ':' + parts[1][2:4] + ':' + parts[1][4:6]

def backup(request):

    if request.method == 'POST':
        form = BackupUploadForm(request.POST, request.FILES)
        if form.is_valid():
            if request.FILES['file'].name.endswith('.tgz'):
                with open(os.path.join(settings.DB_BACKUP_ROOT, request.FILES['file'].name), 'wb+') as destination:
                    for chunk in request.FILES['file'].chunks():
                        destination.write(chunk)

                cmd = 'tar xvfz ' + request.FILES['file'].name
                p =subprocess.Popen(cmd, stdout = subprocess.PIPE, cwd = settings.DB_BACKUP_ROOT, shell = True)
                print p.stdout.read()
                
                cmd = 'rm ' + request.FILES['file'].name
                p =subprocess.Popen(cmd, stdout = subprocess.PIPE, cwd = settings.DB_BACKUP_ROOT, shell = True)
                print p.stdout.read()
                
            else:
                msg = 'Bad file uploaded, should end with: .tgz'
            
    else:
        form = BackupUploadForm()

    files = os.listdir(settings.DB_BACKUP_ROOT)
    backups = {}
    for f in files:
        if len(f) < 5:
            continue

        if f[-3:] == '.db':
            d = f[:-3]
            if d not in backups:
                backups[d] = {'date': niceDate(d), 'backupFile': f[:-3]}
        if f[-4:] == '.rev':
            d = f[:-4]
            if d not in backups:
                backups[d] = {'date': niceDate(d), 'backupFile': f[:-4]}

            fd = open(os.path.join(settings.DB_BACKUP_ROOT, f), 'r')
            rev = fd.read()
            fd.close()

            backups[d]['rev'] = rev.strip()

        if f[-4:] == '.tgz':
            d = f[21:-4]
            if d not in backups:
                backups[d] = {'date': niceDate(d), 'backupFile': f[:-4]}

            backups[d]['download'] = f
            
        print f

    print backups.keys()

    items = []

    if len(backups) > 0:
        keys = backups.keys()
        keys.sort()
        keys.reverse()
        for k in keys:
            print k
            items.append(backups[k])

    p =subprocess.Popen('git rev-list --max-count=1 HEAD', stdout = subprocess.PIPE, cwd = '/home/pi/source/home_auto/', shell = True)

    currentRev = p.stdout.read().strip()

    return render(request, 'admin/core_backup.html', {'items': items, 'currentRev': currentRev, 'form': form})
    
    
def doBackup(request):
    basefile = os.path.splitext(datetime.datetime.now().isoformat('_'))[0].replace(':', '')
    basepath = os.path.join(settings.DB_BACKUP_ROOT, basefile)
    dumpFile = basepath + '.db'
    cmd = 'mysqldump --user=' + settings.DATABASES['default']['USER'] + ' --password=' + settings.DATABASES['default']['PASSWORD'] + ' ' + settings.DATABASES['default']['NAME'] + ' --add-drop-database --result-file=' + dumpFile
    print 'Executing:', cmd
    subprocess.Popen(cmd, shell = True).wait()

    p =subprocess.Popen('git rev-list --max-count=1 HEAD', stdout = subprocess.PIPE, cwd = '/home/pi/source/home_auto/', shell = True)

    fd = open(basepath + '.rev', 'w')
    fd.write(p.stdout.read())
    fd.write('\n')
    fd.close()

    cmd = 'tar czvf ' + 'automagically_backup_' + basefile + '.tgz ' + basefile + '*'
    print 'Executing', cmd
    p =subprocess.Popen(cmd, stderr = subprocess.STDOUT, stdout = subprocess.PIPE, cwd = settings.DB_BACKUP_ROOT, shell = True)

    print p.stdout.read()

    return redirect('/backup/')

def showBackup(request, backup):
    basefile = os.path.join(settings.DB_BACKUP_ROOT, backup)

    p =subprocess.Popen('git rev-list --max-count=1 HEAD', stdout = subprocess.PIPE, cwd = '/home/pi/source/home_auto/', shell = True)

    currentRev = p.stdout.read().strip()

    print basefile

    print os.path.exists(basefile + '.db'), os.path.exists(basefile + '.rev')

    if os.path.exists(basefile + '.db') and os.path.exists(basefile + '.rev'):
        print 'Possible'
        state = 'POSSIBLE'
        fd = open(basefile + '.rev', 'r')
        rev = fd.read()
        fd.close()

        
    else:

        state = 'IMPOSSIBLE'
        rev = ''

    return render(request, 'admin/core_backup_show.html', {'rev': rev, 'currentRev': currentRev, 'backupDate': niceDate(backup), 'state': state, 'backup': backup})    
    

def restoreBackup(request, backup):
    backupFile = os.path.join(settings.DB_BACKUP_ROOT, backup) + '.db'
    restoreOutput = ''

    if os.path.exists(backupFile):
        cmd = 'mysql --verbose --user=' + settings.DATABASES['default']['USER'] + ' --password=' + settings.DATABASES['default']['PASSWORD'] +' ' + settings.DATABASES['default']['NAME'] + ' < ' + backupFile

        print cmd
        p =subprocess.Popen(cmd, stdout = subprocess.PIPE, cwd = '/home/pi/source/home_auto/site/', shell = True)

        restoreOutput += '\n\n*************************************\n'
        restoreOutput += 'Executing: ' + cmd + '\n'

        restoreOutput += p.stdout.read()

        migrations = ['python manage.py migrate core',
                      'python manage.py migrate signals',
                      'python manage.py migrate settings']

        for cmd in migrations:
            restoreOutput += '\n\n*************************************\n'
            restoreOutput += 'Executing: ' + cmd + '\n'            
            print cmd
            p =subprocess.Popen(cmd, stdout = subprocess.PIPE, cwd = '/home/pi/source/home_auto/site/', shell = True)

            restoreOutput += p.stdout.read()

    else:
        return redirect('/backup/')

    state = 'DONE'

    return render(request, 'admin/core_backup_show.html', {'state': state, 'backup': backup, 'restoreOutput': restoreOutput})    


    
def read_config(request):

    count = 0

    fd = open('/etc/tellstick.conf')

    lines = fd.readlines()

    id = ''
    name = ''
    protocol = ''
    model = ''
    house = ''
    unit = ''
    code = ''

    for i in range(len(lines)):
        if lines[i].strip().startswith('device {'):
            if id != '':
                count += 1
                addDevice(id, name, protocol, model, house, unit, code)
            id = ''
            name = ''
            protocol = ''
            model = ''
            house = ''
            unit = ''
            code = ''
            if debug:
                print 'new device'

        elif lines[i].strip().startswith('id = '):
            id = lines[i].split('=')[1].strip()
        elif lines[i].strip().startswith('name = '):
            name = lines[i].split('"')[1]
        elif lines[i].strip().startswith('protocol = '):
            protocol = lines[i].split('"')[1]
        elif lines[i].strip().startswith('model = '):
            model = lines[i].split('"')[1]
        elif lines[i].strip().startswith('parameters {'):
            pass
        elif lines[i].strip().startswith('house = '):
            house = lines[i].split('"')[1]
        elif lines[i].strip().startswith('unit = '):
            unit = lines[i].split('"')[1]
        elif lines[i].strip().startswith('code = '):
            code = lines[i].split('"')[1]
        elif lines[i].strip().startswith('#'):
            pass

    if id != '':
        count += 1
        addDevice(id, name, protocol, model, house, unit, code)

    print '/etc/tellstick.conf contains', count, 'devices that has been processe'
    return redirect('/core/')

    
