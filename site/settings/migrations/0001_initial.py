# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SettingsValue'
        db.create_table('settings_settingsvalue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('dataType', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('settings', ['SettingsValue'])

        # Adding model 'Group'
        db.create_table('settings_group', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal('settings', ['Group'])

        # Adding M2M table for field subSettings on 'Group'
        db.create_table('settings_group_subSettings', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('group', models.ForeignKey(orm['settings.group'], null=False)),
            ('settingsvalue', models.ForeignKey(orm['settings.settingsvalue'], null=False))
        ))
        db.create_unique('settings_group_subSettings', ['group_id', 'settingsvalue_id'])

    def backwards(self, orm):
        # Deleting model 'SettingsValue'
        db.delete_table('settings_settingsvalue')

        # Deleting model 'Group'
        db.delete_table('settings_group')

        # Removing M2M table for field subSettings on 'Group'
        db.delete_table('settings_group_subSettings')

    models = {
        'settings.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'subSettings': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['settings.SettingsValue']", 'symmetrical': 'False'})
        },
        'settings.settingsvalue': {
            'Meta': {'object_name': 'SettingsValue'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['settings']