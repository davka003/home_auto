# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Handler'
        db.create_table('signals_handler', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('desc', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('pattern', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('signals', ['Handler'])

        # Adding model 'Transform'
        db.create_table('signals_transform', (
            ('handler_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['signals.Handler'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('signals', ['Transform'])

        # Adding model 'DeviceCommand'
        db.create_table('signals_devicecommand', (
            ('handler_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['signals.Handler'], unique=True, primary_key=True)),
            ('dev', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Device'])),
            ('cmd', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Command'])),
        ))
        db.send_create_signal('signals', ['DeviceCommand'])

    def backwards(self, orm):
        # Deleting model 'Handler'
        db.delete_table('signals_handler')

        # Deleting model 'Transform'
        db.delete_table('signals_transform')

        # Deleting model 'DeviceCommand'
        db.delete_table('signals_devicecommand')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'htmlText': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'signals.devicecommand': {
            'Meta': {'object_name': 'DeviceCommand', '_ormbases': ['signals.Handler']},
            'cmd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'handler_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['signals.Handler']", 'unique': 'True', 'primary_key': 'True'})
        },
        'signals.handler': {
            'Meta': {'object_name': 'Handler'},
            'desc': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pattern': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'signals.transform': {
            'Meta': {'object_name': 'Transform', '_ormbases': ['signals.Handler']},
            'handler_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['signals.Handler']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['signals']