from django.db import models
from core.models import Device, Command, GlobalVariable
from settings.models import Group, SettingsValue, getSettings
from django.conf import settings

from django.db.models.signals import post_save
from django.dispatch import receiver


import datetime
from pytz import timezone
import time

import threading
import Queue

from scanf import scanf_compile
import re

import sys
import os

debug = False

PLUGINS_PATH = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins')

sys.path.append(PLUGINS_PATH)

maximumSignalRecursionLevel = 20 #Max number of repost from a signal (to avoid infinite recursion)

signalQueue = Queue.Queue()


#Post a signal to the queue of signals to process
#returnTrace not functional
def postToQueue(signal, input, returnTrace = False):    

    if isinstance(input, SignalObject):
        repost = True
        level = input.level+1
        origin = input.origin
    else:
        repost = False
        level = 0
        origin = input.strip()
#        t0 = time.time()
    if level >= maximumSignalRecursionLevel:
        print 'Error maximumSignalRecursionLevel reached, not allowed to post more to queue'
        return []
        

    s = SignalObject(signal.strip(), origin, level)
    #print 'New Signal in queue:', s, repost
    signalQueue.put(s)

    return []

handlerCache = []

def _fillHandlerCache():
    print 'Update cached table of Handlers'
    global handlerCache
    handlerCache = []
    for h in Handler.objects.all():
        handlerCache.append((h, re.compile(h.regexp)))


class SignalProcessingThread(threading.Thread): 
    def __init__(self):
        if len(handlerCache) == 0:
            print 'Empty handler cache, fill it'
            _fillHandlerCache()

        threading.Thread.__init__(self)

    def run(self):
        print 'SignalProcessingThread started'
        while True:
#            print 'Doing a get from the queue'
            s = signalQueue.get()

            if s == None:
                print 'Got a none from signalQueue'
                continue

#            print 'Got signal from Queue', s
            
            plugins.processSignal(s)

            for h, regexp in handlerCache:
                found = regexp.match(s.content)
                if found:
                    if hasattr(h, 'transform'):
                        h.transform.doProcess(s, found)
                    elif hasattr(h, 'devicecommand'):
                        h.devicecommand.doProcess(s, found)
                    elif hasattr(h, 'storeglobalvariable'):
                        h.storeglobalvariable.doProcess(s, found)
                    elif hasattr(h, 'findrepeat'):
                        h.findrepeat.doProcess(s, found)   

            signalQueue.task_done()

class SignalObject():
    def __init__(self, content, origin, level):
        self.content = content
        self.origin = origin
        self.level = level
        self.timestamp = time.time()

    def __str__(self):
        return '  '*self.level + self.origin + '#' + self.content

re_cache = {}
re_cache_size = 500


class Handler(models.Model):
    desc = models.CharField(max_length=255, verbose_name = 'Description')
    pattern = models.CharField(max_length=255, verbose_name = 'Pattern to match')
    patternIsRegularExpression = models.BooleanField(default = False, verbose_name = 'Pattern is a regular expression (not a scanf string)')
    regexp = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return self.desc

    
    def process(self, signal, trace):
        if self.regexp not in re_cache:
            format_re = re.compile(self.regexp)

            if len(re_cache) > re_cache_size:
                re_cache.clear()

            re_cache[self.regexp] = format_re

        found = re_cache[self.regexp].match(signal.content)

        if found:
            if trace:
                signalTrace.append('  '*(signal.level+1) + 'Processed by Handler ' + self.desc)
            if hasattr(self, 'transform'):
                return self.transform.doProcess(signal, found)
            elif hasattr(self, 'devicecommand'):
                return self.devicecommand.doProcess(signal, found)
            elif hasattr(self, 'storeglobalvariable'):
                return self.storeglobalvariable.doProcess(signal, found)
            elif hasattr(self, 'findrepeat'):
                return self.findrepeat.doProcess(signal, found)
            
        return -1

    def clean(self):
        if self.patternIsRegularExpression:
            self.regexp = self.pattern
        else:
            self.regexp, casts = scanf_compile(self.pattern)
        
        
    def save(self):
        super(Handler, self).save()
        _fillHandlerCache()

class Transform(Handler):
    output = models.TextField(max_length=255, verbose_name = 'Send the following signal(s) on match')

    def doProcess(self, signal, match):
        for s in self.output.splitlines():
            if s.strip() != '':
                i = 0
                for v in match.groups():
                    i += 1
                    s = s.replace('$' + str(i), v)

                postToQueue(s, signal)




class DeviceCommand(Handler):
    dev = models.ForeignKey(Device, verbose_name = 'Device')
    cmd = models.ForeignKey(Command, verbose_name = 'Command')

    def doProcess(self, signal, match):
        self.dev.execute(self.cmd)

        
class StoreGlobalVariable(Handler):
    ParsedVariable = models.PositiveIntegerField(default = 1, verbose_name = 'Parsed variable to store')
    Variable = models.ForeignKey(GlobalVariable, verbose_name = 'Variable to update')    
    Constant = models.CharField(max_length=80, default = '', blank=True, verbose_name = 'Store this constant instead of parsed variable')
    
    def doProcess(self, signal, match):
        try:
            if self.Constant == '':
                self.Variable.updateValue(match.groups()[self.ParsedVariable-1])
            else:
                self.Variable.updateValue(self.Constant)
            
        except:
            print 'Error storing in global variable'



findRepeatTemp = {}

class FindRepeat(Handler):
    nrRepeats = models.PositiveIntegerField(default = 2, verbose_name = '# of repeats to trigger')
    maxWaitTimeMs = models.PositiveIntegerField(default = 1000, verbose_name = 'Max time to get all the repeats (ms)')
    holdOffTimeMs = models.PositiveIntegerField(default = 10000, verbose_name = 'Time to wait after triggered before finding new repeats (ms)')
        
    output = models.TextField(max_length=512, verbose_name = 'Signal(s) to send on trigger')

    def doProcess(self, signal, match):
#        print 'FindRepeat'
        n = datetime.datetime.now()

        tempData = findRepeatTemp.get(self.id, None)

        if self.id not in findRepeatTemp:
            tempData = (n, n, 1)
            #print 'Started new/first FindRepeat'

        elif tempData[2] == 0 and tempData[0] + datetime.timedelta(milliseconds = self.holdOffTimeMs) > datetime.datetime.now():
            #print 'Holdoff'
            pass

        elif tempData[0] + datetime.timedelta(milliseconds = self.maxWaitTimeMs) < datetime.datetime.now():
            #To much time have elapsed
            tempData = (n, n, 1)
            
            #print 'Started new FindRepeat'

        elif tempData[2] + 1 >= self.nrRepeats:

            for s in self.output.splitlines():
                if s.strip() != '':
                    i = 0
                    for v in match.groups():
                        i += 1
                        s = s.replace('$' + str(i), v)

                    postToQueue(s, signal)

            tempData = (n, n, 0)
            #print 'Repeat detected reset object\n\n\n'

        else:
            tempData = (tempData[0], n, tempData[2] + 1)
            #print 'Found another repeat', tempData[2], self.nrRepeats


        findRepeatTemp[self.id] = tempData

@receiver(post_save, dispatch_uid=__name__, sender=SettingsValue)
def settingsChanged(sender, **kwargs):
    
    if debug:
        print kwargs['instance'], 'updated'

    s = SignalObject('configuration,changed', 'system', 0)    

    try:
        plugins.alertSettingsChanged(kwargs['instance'].group.name, s)
    except:
        pass #This will fail on startup as plugins call this on import


if (len(sys.argv) >= 2 and sys.argv[1].lower() == 'runserver') or (len(sys.argv) < 2):
    signalHandler = SignalProcessingThread()
    signalHandler.start()

#This should be done last
import plugins
