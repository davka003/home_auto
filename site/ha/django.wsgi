import os
import sys

sys.path.append('/home/pi/django_site/home_auto')

os.environ['DJANGO_SETTINGS_MODULE'] = 'ha.settings_apache'

#PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))

#sys.path.append(PROJECT_PATH)
#sys.path.append(os.path.join(PROJECT_PATH,".."))

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()
