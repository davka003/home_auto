from django.db import models
from settings.models import getSettings
import time
import os

PLUGIN_NAME = os.path.splitext(os.path.split(__file__)[1])[0] #This will be the filename without .py

global generalSettings
generalSettings = {}

def signalHandler(signal):
    global generalSettings
    if not generalSettings or 'configuration,changed' == signal.content:
        generalSettings = getSettings(PLUGIN_NAME)

def getSetting(setting):
    global generalSettings
    if not generalSettings:
        generalSettings = getSettings(PLUGIN_NAME)

    return generalSettings.get(setting, None)
    
            
def init():
    settings = {'Location Latitude':   ('float', 65.57777),
                'Location Longitude': ('float', 22.2574)}

    return ('general settings', settings, signalHandler, None)

