from django.db import models
from settings.models import Group, SettingsValue, updateSettings

import threading

import sys
import os
import time

PLUGINS_PATH = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins')

workers = []


def registerSignalWorker(prefix, func, moduleName):
    print 'Worker registered for prefix', '"' + prefix + '"', func
    workers.append((prefix, func, moduleName))


def processSignal(s):
    for prefix, handler, moduleName in workers:
        if s.content.startswith(prefix):
            handler(s)

def alertSettingsChanged(module, signal):
    for prefix, handler, moduleName in workers:
        if moduleName == module:
            print 'Alert plugin of config changes'
            handler(signal)
            break


class PluginThread(threading.Thread):
    def __init__(self, plugin, func):
        self.func = func
        self.plugin = plugin
        threading.Thread.__init__(self)

    def run(self):
        time.sleep(10) #This is very ugly to have the server fully started before letting the plugins run
        print 'Thread for', self.plugin, 'started'
        self.func()


if (len(sys.argv) >= 2 and sys.argv[1].lower() == 'runserver') or (len(sys.argv) < 2):
    for item in os.listdir(PLUGINS_PATH):
        if os.path.isfile(os.path.join(PLUGINS_PATH, item)):
            if item.lower().endswith('.py') and item != 'plugins.py':
                moduleName = item[:-3]
                print 'Found plugin', moduleName, '----------------'
                try:
                    prefix, settings, workerFunc, threadFunc = __import__(moduleName, globals()).init()
                    if settings:
                        updateSettings(moduleName, settings)

                    if workerFunc and prefix != None:
                        registerSignalWorker(prefix, workerFunc, moduleName)

                    if threadFunc:
                        t = PluginThread(moduleName, threadFunc)
                        t.start()

                    print 'Plugin init done --------------------'
                except:
                    print 'Failed initilize plugin'
                    raise



