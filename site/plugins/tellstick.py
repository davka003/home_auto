from django.db import models
import core.td
import signals.models
import time
import Queue


PLUGIN_NAME = 'tellstick'

debug = False
init_done = False
raw_enabled = False
callbacks = {}

def sensorEvent(protocol, model, id, dataType, value, timestamp, callbackId):
    if debug:
        print 'sensorEvent happend'

    if init_done:
        signals.models.postToQueue('tellstick,sensor,protocol:' + protocol + ',id:' + str(id) + ',' + core.td.sensorValueTypeReadable[dataType] + ',value:' + str(value), PLUGIN_NAME)

def deviceEvent(deviceId, method, data, callbackId):
    if debug:
        print 'deviceEvent happend'

    if init_done:
        signals.models.postToQueue('tellstick,device:' + core.td.getName(deviceId) + ',id:' + str(deviceId) + ',method:' + core.td.methodsReadable[method] + ',data:' + str(data), PLUGIN_NAME)




def rawDeviceEvent(data, controllerId, callbackId):
    if debug:
        print 'RawDeviceEvent', data

    global raw_enabled
    if raw_enabled and raw_enabled < time.time():
        raw_enabled = False
        if debug:
            print 'Unregister raw callbacks'
        core.td.unregisterCallback(callbacks['raw'])


    if init_done and raw_enabled:
        signals.models.postToQueue('tellstick,raw:' + data, PLUGIN_NAME)


workQueue = Queue.Queue()

def signalHandler(signal):
    if signal.content.startswith('tellstick,do:'):
        for i in signal.content[13:].split(';'):
            if i != '':
                workQueue.put(i)
    elif signal.content.startswith('tellstick,rawenable:'):

        global raw_enabled
        if not raw_enabled:
            callbacks['raw'] = core.td.registerRawDeviceEvent(rawDeviceEvent)
            if debug:
                print 'Register raw callbacks', callbacks['raw']

        raw_enabled = time.time() + int(signal.content.split(':')[1])
        if debug:
            print 'Raw enabled in', int(signal.content.split(':')[1]), 'seconds'



def threadFunc():
    while(1):
        try:
            s = workQueue.get()

            if s == None:
                print 'Got a none from tellstick workQueue'
                workQueue.task_done()
                continue

            s = s.split(',')
            if debug:
                print 'Time to execute tellstick command', s
            cmd = s[1].lower()
            if cmd == 'on':
                core.td.turnOn(int(s[0]))
            elif cmd == 'off':
                core.td.turnOff(int(s[0]))
            elif cmd == 'bell':
                core.td.bell(int(s[0]))
            elif cmd == 'dim':
                core.td.dim(int(s[0]), int(int(s[2])*2.55))
            elif cmd == 'up':
                core.td.up(int(s[0]))
            elif cmd == 'down':
                core.td.down(int(s[0]))
            elif cmd == 'stop':
                core.td.stop(int(s[0]))
            elif cmd == 'learn':
                core.td.learn(int(s[0]))
            else:
                print 'Unknown command', s

                            

            workQueue.task_done()

        except:
            print 'Error executing tellstick command'
    


def init():
    global init_done
    init_done = True

    core.td.init()

    callbacks['sensor'] = core.td.registerSensorEvent(sensorEvent)
    callbacks['device'] = core.td.registerDeviceEvent(deviceEvent)


    if debug:
        print 'registerSensorEvent', callbacks['sensor']
        print 'registerDeviceEvent', callbacks['device']

    settings = {}

    return (PLUGIN_NAME, settings, signalHandler, threadFunc)

