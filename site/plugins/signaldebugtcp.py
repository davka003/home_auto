from django.db import models
import signals.models
from settings.models import getSettings
import time
import os
import socket
import select
from curses.ascii import isprint
import datetime

PLUGIN_NAME = os.path.splitext(os.path.split(__file__)[1])[0] #This will be the filename without .py

debug = False

global currentSettings
currentSettings = {}

server = None
conn = None
addr = None
connected = False
doServerRestart = False


def printable(input):
    return ''.join(char for char in input if isprint(char))

def signalHandler(signal):
    global currentSettings
    if not currentSettings or 'configuration,changed' == signal.content:
        currentSettings = getSettings(PLUGIN_NAME)
        doServerRestart = True
        print 'Signaldebugtcp got configuration updated message'

  
    if currentSettings.get('Debug to socket', False):
        global connected
        global conn
        global server
        if connected and conn:
            try:
                conn.send('%7.3f:  %s\r\n' %(time.time(), str(signal)))
            except socket.error, msg:
                if conn:
                    connected = False
                    try:
                        conn.shutdown(socket.SHUT_RDWR)
                    except:
                        pass
                    try:
                        conn.close()
                    except:
                        pass
                    server = None
                    conn = None
                    doServerRestart = True
            

def threadFunc():

    global conn
    global addr
    global server
    global connected
    global doServerRestart

    currentSettings = getSettings(PLUGIN_NAME)

    while 1:
        doServerRestart = False
        
        if debug:
            print 'signaldebug threadfunc', currentSettings

        if currentSettings.get('Debug to socket', False):
            try:
                server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                server.bind(('', currentSettings.get('Debug socket', 8111)))
                server.listen(1)
            except socket.error, msg:
                signals.models.postToQueue('error in signaldebug when starting debug socket: ' + str(msg), PLUGIN_NAME)
                server = None
                time.sleep(5)
                continue

                

            server.settimeout(0.3)
            if debug:
                print 'Accepting incomming connection on', currentSettings.get('Debug socket', 8111)
            while not doServerRestart:
                try:
                    conn, addr = server.accept()
                    connected = True
                    break
                except:
                    pass

            if debug:
                print 'Connected'

            while not doServerRestart:
                try:
                    inputready, outputready, exceptready = select.select([conn], [], [])
                    data = conn.recv(256)

                    dataClean = printable(data).strip()
                    if dataClean and dataClean != "'":
                        signals.models.postToQueue(dataClean, PLUGIN_NAME)
                
                except socket.error, msg:
                    if not doServerRestart:
                        conn.close()
                        connected = False
                        server = None
                    break
                except:
                    break
                    

            connected = False
        else:
            time.sleep(5)


def init():
    settings = {'Debug to socket': ('bool', False),
                'Debug socket':    ('int',  8111)}

    return ('', settings, signalHandler, threadFunc)

