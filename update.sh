#!/bin/bash
cd /home/pi/source/home_auto
git pull
cd /home/pi/source/home_auto/site
python manage.py migrate core
python manage.py migrate signals
python manage.py migrate settings
python manage.py collectstatic --noinput
echo " " > plugins/signaldebug.txt
chmod 666 plugins/signaldebug.txt
echo "*************************************************"
echo "*                                               *"
echo "* This is the last time you are able to update  *"
echo "* like this, for future updates you need to     *"
echo "* start using a raspbian based image that you   *"
echo "* can download from:                            *"
echo "* automagically.weebly.com                      *"
echo "* This last update makes it possible for you    *"
echo "* to download your current configuration so     *"
echo "* that you dont have to make it again           *"
echo "* Visit [rpi ip]/backup/ for that.              *"
echo "*                                               *"
echo "* It is now adviced to reboot the RPi           *"
echo "*                                               *"
echo "*************************************************"
